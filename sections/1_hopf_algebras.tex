\chapter{Hopf Algebras}





\section{Bialgebras}


\begin{convention}
	Let~$\kf$ be a commutative ring with multiplicative neutral element~$1$.
	(Later on~$\kf$ will always be a field.)
\end{convention}


\begin{notation}
	For every two~\modules{$\kf$}~$V$ and~$W$, the twist~homomorphism\index{twist homomorphism} from~$V \tensor W$ to~$W \tensor V$ is denoten by~$\tau_{V, W}$\glsadd{twist}.
	It is given by
	\[
		\tau_{V, W}(v \tensor w)
		=
		w \tensor v
	\]
	for all~$v \in V$,~$w \in W$.
\end{notation}


\begin{definition}
	\leavevmode
	\begin{enumerate}
		\item
			A~\defemph{\algebra{$\kf$}}\index{algebra} is a triple~$(A, m, u)$ consting of a~\module{$\kf$}~$A$ and two~\linear{$\kf$} maps
			\[
				m \colon A \tensor A \to A \,,
				\quad
				u \colon \kf \to A \,,
			\]
			such that the two diagrams
			\[
				\begin{tikzcd}[sep = large]
					A \tensor A \tensor A
					\arrow{r}[above]{m \tensor \id}
					\arrow{d}[left]{\id \tensor m}
					&
					A \tensor A
					\arrow{d}[right]{m}
					\\
					A \tensor A
					\arrow{r}[above]{m}
					&
					A
				\end{tikzcd}
			\]
			and
			\[
				\begin{tikzcd}[sep = large]
					\kf \tensor A
					\arrow{r}[above]{u \tensor \id}
					\arrow[bend right]{dr}[below left]{\sim}
					&
					A \tensor A
					\arrow{d}[right]{m}
					&
					A \tensor \kf
					\arrow{l}[above]{\id \tensor u}
					\arrow[bend left]{dl}[below right]{\sim}
					\\
					{}
					&
					A
					&
					{}
				\end{tikzcd}
			\]
			commute.
		\item
			The map~$m$\glsadd{multiplication} is the \defemph{multiplication}\index{multiplication} of the algebra, and the map~$u$\glsadd{unit} is its \defemph{unit}\index{unit}
		\item
			The first diagram is the \defemph{associativity diagram}\index{associativity}, and the second diagram is the \defemph{unital diagram}\index{unital}.
		\item
			A~\algebra{$\kf$} is \defemph{commutative}\index{commutative} if the following \defemph{commutativity diagrams} commutes:
			\[
				\begin{tikzcd}[row sep = large]
					A \tensor A
					\arrow{rr}[above]{\tau_{A, A}}
					\arrow{dr}[below left]{m}
					&
					{}
					&
					A \tensor A
					\arrow{dl}[below right]{m}
					\\
					{}
					&
					A
					&
					{}
				\end{tikzcd}
			\]
	\end{enumerate}
\end{definition}


\begin{fluff}
	By reversing the arrows in the above definition of a~\algebra{$\kf$}, we arrive at the definition of a~\coalgebra{$\kf$}.
\end{fluff}


\begin{definition}
	\leavevmode
	\begin{enumerate}
		\item
			A~\coalgebra{$\kf$}\index{coalgebra} is a triple~$(C, \Delta, \varepsilon)$ consisting of a~\module{$\kf$}~$C$ and two~\linear{$\kf$} maps
			\[
				\Delta \colon C \to C \tensor C \,,
				\quad
				\varepsilon \colon C \to \kf
			\]
			such that the two diagrams
			\[
				\begin{tikzcd}[sep = large]
					C
					\arrow{r}[above]{\Delta}
					\arrow{d}[left]{\Delta}
					&
					C \tensor C
					\arrow{d}{\id \tensor \Delta}
					\\
					C \tensor C
					\arrow{r}{\Delta \tensor \id}
					&
					C \tensor C \tensor C
				\end{tikzcd}
			\]
			and
			\[
				\begin{tikzcd}[sep = large]
					{}
					&
					C
					\arrow{d}[right]{\Delta}
					\arrow[bend right]{dl}[above left]{\sim}
					\arrow[bend left]{dr}[above right]{\sim}
					&
					{}
					\\
					\kf \tensor C
					&
					C \tensor C
					\arrow{l}[above]{\varepsilon \tensor \id}
					\arrow{r}[above]{\id \tensor \varepsilon}
					&
					C \tensor \kf
				\end{tikzcd}
			\]
			commute.
		\item
			The map~$\Delta$\glsadd{comultiplication} is the \defemph{comultiplication}\index{comultiplication} of the coalgebra, and the map~$\varepsilon$\glsadd{counit} is its \defemph{counit}\index{counit}.
		\item
			The first diagram is the \defemph{coassociativity diagram}\index{coassociativity}, and the second diagram is the~\defemph{counital diagram}\index{counital}.
		\item
			A coalgebra~$(C, \Delta, \varepsilon)$ is \defemph{cocommutative}\index{cocommutative} if the following \defemph{cocommutativity diagram} commutes:
			\[
				\begin{tikzcd}[row sep = large]
					{}
					&
					C
					\arrow{dl}[above left]{\Delta}
					\arrow{dr}[above right]{\Delta}
					&
					{}
					\\
					C \tensor C
					\arrow{rr}[above]{\tau_{C, C}}
					&
					{}
					&
					C \tensor C
				\end{tikzcd}
			\]
	\end{enumerate}
\end{definition}


\begin{definition}
	Let~$(C, \Delta, \varepsilon)$ and~$(C', \Delta', \varepsilon')$ be two~\coalgebras{$\kf$}.
	A \defemph{homomorphism of coalgebras}\index{homomorphism!of coalgebras} from~$(C, \Delta, \varepsilon)$ to~$(C', \Delta', \varepsilon')$ is a~\linear{$\kf$} map~$f$ from~$C$ to~$C'$ such that the following two diagrams commute:
	\[
		\begin{tikzcd}[sep = large]
			C
			\arrow{r}[above]{f}
			\arrow{d}[left]{\Delta}
			&
			C'
			\arrow{d}[right]{\Delta'}
			\\
			C \tensor C
			\arrow{r}[above]{f \tensor f}
			&
			C' \tensor C'
		\end{tikzcd}
		\qquad
		\begin{tikzcd}[row sep = large]
			C
			\arrow{rr}[above]{f}
			\arrow{dr}[below left]{\varepsilon}
			&
			{}
			&
			C'
			\arrow{dl}[below right]{\varepsilon'}
			\\
			{}
			&
			\kf
			&
			{}
		\end{tikzcd}
	\]
\end{definition}


\begin{example}
	\label{first structure on k[x]}
	Let~$C$ be the vector space~$\kf[x]$.
	There exist (by the universal property of the polynomial algebra) a unique homomorphism of~\algebras{$\kf$}~$\Delta$ from~$C$ to~$C \tensor C$ with
	\[
		\Delta(x)
		=
		x \tensor 1 + 1 \tensor x \,.
	\]
	Let~$\varepsilon$ be the unique homomorphism of~\algebras{$\kf$} from~$C$ to~$\kf$ given by
	\[
		\varepsilon(x) = 0 \,.
	\]
	The triple~$(C, \Delta, \varepsilon)$ is a cocommutative coalgebra.

	For the coassociativity we need to check the equality
	\[
		(\Delta \tensor \id) \circ \Delta
		=
		(\id \tensor \Delta) \circ \Delta \,.
	\]
	Both sides of this equation are homomorphisms of~\algebras{$\kf$} from~$C$ to~$C \tensor C \tensor C$.
	It therefore sufficies to check this equality on the algebra generator~$x$ of~$C$.
	On this generator we have
	\[
		( (\Delta \tensor \id) \circ \Delta )(x)
		=
		x \tensor 1 \tensor 1
		+ 1 \tensor x \tensor 1
		+ 1 \tensor 1 \tensor x
		=
		( (\id \tensor \Delta) \circ \Delta )(x) \,.
	\]
	This shows the commutativity of the coassociativity diagram.
	
	All maps in the counitality diagram for~$C$ are homomorphisms of algebras.
	It therefore sufficies to check the commutativity of the counitality diagram for~$C$ on the algebra generater~$x$ of~$C$.
	We have
	\begin{align*}
		\SwapAboveDisplaySkip
		( (\varepsilon \tensor \id) \circ \Delta )(x)
		&=
		(\varepsilon \tensor \id)( \Delta(x) )
		\\
		&=
		(\varepsilon \tensor \id)( x \tensor 1 + 1 \tensor x )
		\\
		&=
		\varepsilon(x) \tensor \id(1)
		+ \varepsilon(1) \tensor \id(x)
		\\
		&=
		0 \tensor 1 + 1 \tensor x
		\\
		&=
		1 \tensor x \,,
	\shortintertext{and similarly}
		( (\id \tensor \varepsilon) \circ \Delta )(x)
		&=
		(\id \tensor \varepsilon)( \Delta(x) )
		\\
		&=
		(\id \tensor \varepsilon)( x \tensor 1 + 1 \tensor x )
		\\
		&=
		\id(x) \tensor \varepsilon(1)
		+ \id(1) \tensor \varepsilon(x)
		\\
		&=
		x \tensor 1 + 1 \tensor 0
		\\
		&=
		x \tensor 1 \,,
	\end{align*}
	Under the isomorphisms of~$\kf \tensor C$ and~$C \tensor \kf$ to~$C$ we see that both~$1 \tensor x$ and~$x \tensor 1$ correspond to the same element~$x$ of~$C$.
	This shows the commutativity of the counitality diagram.
	
	For the cocommutativity we need to check that
	\[
		\tau_{C, C} \circ \Delta
		=
		\Delta
	\]
	both sides of this equation are homomorphisms of~\algebras{$\kf$}.
	It therefore sufficies to check the equality on the algebra generator~$x$ of~$C$.

	On this generator we have
	\begin{align*}
		( \tau_{C, C} \circ \Delta )(x)
		&=
		\tau_{C, C}( \Delta(x) )
		\\
		&=
		\tau_{C, C}( x \tensor 1 + 1 \tensor x )
		\\
		&=
		1 \tensor x + x \tensor 1
		\\
		&=
		x \tensor 1 + 1 \tensor x
		\\
		&=
		\Delta(x) \,,
	\end{align*}
	as desired.
\end{example}


\begin{warning}
	The notions of a~\algebra{$\kf$} and of a~\coalgebra{$\kf$} may seem dual to each other, but they are not completely so.
	We will see this now in more detail.
\end{warning}


\begin{convention}
	For every~\module{$\kf$}~$V$ we denote the \defemph{dual module}~$\Hom_{\kf}(V, \kf)$\index{dual module} by~$V^\check$\glsadd{dual module}.
\end{convention}


\begin{recall}
	\label{dual of tensor product}
	\leavevmode
	\begin{enumerate}
		\item
			For every two~\modules{$\kf$} we have a homomorphisms of~\modules{$\kf$}~$\Phi_{V,W}$ from~$V^\check \tensor W^\check$ to~$(V \tensor W)^\check$ given by
			\[
				\Phi_{V,W}(f \tensor g)(v \tensor w)
				=
				f(v) g(w)
			\]
			for all~$f \in V^\check$,~$g \in W^\check$,~$v \in V$,~$w \in W$.
			If~$\kf$ is a field, then the linear map~$\Phi_{V,W}$ is injective, and it is an isomorphism if and only if at least one of the two vector spaces~$V$ or~$W$ is finite-dimensional.
		\item
			The map
			\[
				\Psi
				\colon
				\kf
				\to
				\kf^\check \,,
				\quad
				\lambda
				\mapsto
				(x \mapsto \lambda x)
			\]
			in an isomorphism of~\modules{$\kf$}.
	\end{enumerate}
\end{recall}


\begin{proposition}
	\label{dual of coalgebra is algebra}
	Let~$(C, \Delta, \varepsilon)$ be a coalgebra.
	Using the notation from~\cref{dual of tensor product} let~$m$ be the composite
	\[
		m
		\colon
		C^\check \tensor C^\check
		\xto{\Phi_{C,C}}
		(C \tensor C)^\check
		\xto{\Delta^\check}
		C^\check \,,
	\]
	and let~$u$ be the composite
	\[
		u
		\colon
		\kf
		\xto{\Psi}
		\kf^\check
		\xto{\varepsilon^\check}
		C^\check \,.
	\]
	\begin{enumerate}
		\item
			The triple~$(C^\check, m, u)$ is a~\algebra{$\kf$}.
		\item
			Suppose that~$(C, \Delta, \varepsilon)$ is cocommutative.
			Then~$(C^\check, m, u)$ is commutative.
		\qed
	\end{enumerate}
\end{proposition}


\begin{proposition}
	\label{dual of fd algebra is a coalgebra}
	Suppose that~$\kf$ is a field and let~$(A, m, u)$ be a finite-dimensional~\algebra{$\kf$}.
	Using the notation from \cref{dual of tensor product} let~$\Delta$ be the composite
	\[
		\Delta
		\colon
		A^\check
		\xto{m^\check}
		(A \tensor A)^\check
		\xto{\Phi_{A, A}^{-1}}
		A^\check \tensor A^\check
	\]
	and let~$\varepsilon$ be the composite
	\[
		\varepsilon
		\colon
		A^\check
		\xto{u^\check}
		\kf^\check
		\xto{\Psi^{-1}}
		\kf \,.
	\]
	\begin{enumerate}
		\item
			The triple~$(A^\check, \Delta, \varepsilon)$ is a coalgebra.
		\item
			Suppose that~$(A, m, u)$ is commutative.
			Then~$(A^\check, \Delta, \varepsilon)$ is cocommutative.
		\qed
	\end{enumerate}
\end{proposition}


\begin{remark}
	\begin{enumerate}
		\item
			We see that the approach from \cref{dual of fd algebra is a coalgebra} does not work for an infinite-dimensional~\algebra{$\kf$} since the linear map~$\Phi_{A,A}$ from~$A^\check$ to~$A^\check$ is not bijective.
			We will later see that we can instead work with the \enquote{finite dual}\index{finite dual} given by
			\[
				A^\circ
				=
				\{
					f \in A^\check
				\suchthat
					\text{$f$ factors through some finite-dimensional quotient of~$A$}
				\} \,.
			\]
		\item
			However, we may restrict our attention to both finite-dimensional~\algebras{$\kf$} and finite-dimensional~\coalgebras{$\kf$}.
			Then the two constructions from~\cref{dual of coalgebra is algebra} and~\cref{dual of fd algebra is a coalgebra} result in mutually essentially-inverse equivalences between the category of finite-dimensional~\algebras{$\kf$} and the category of finite-dimensional~\coalgebras{$\kf$}.
	\end{enumerate}
\end{remark}


\begin{definition}
	A~\defemph{\bialgebra{$\kf$}}\index{bialgebra} quintuple~$(B, m, u, \Delta, \varepsilon)$ consisting of a~\module{$\kf$}~$B$ and~\linear{$\kf$} maps
	\[
		m \colon B \tensor B \to B \,,
		\qquad
		u \colon \kf \to B \,,
		\qquad
		\Delta \colon B \to B \tensor B \,,
		\qquad
		\varepsilon \colon B \to \kf
	\]
	such that the following properties hold.
	\begin{enumerate}
		\item
			The triple~$(B, m, u)$ is an algebra.
		\item
			The triple~$(B, \Delta, \varepsilon)$ is a coalgebra.
		\item
			\label{comultiplication and counit are algebra homos}
			Both~$\Delta$ and~$\varepsilon$ are homomorphisms of algebras.
		\item
			Both~$m$ and~$u$ are homomorphisms of coalgebras.
			\label{multiplication and unit are coalgebra homos}
	\end{enumerate}
\end{definition}


\begin{remark}
	Conditions~\ref{comultiplication and counit are algebra homos} and~\ref{multiplication and unit are coalgebra homos} are actually equivalent.
\end{remark}


\begin{example}
	We have constructed a bialgebra-structure on~$\kf[x]$ in \cref{first structure on k[x]}.
\end{example}


\begin{definition}
	A~\bialgebra{$\kf$}~$(H, m, u, \Delta, \varepsilon)$ is a~\hopfalgebra{$\kf$}\index{Hopf algebra} if there exist a~\linear{$\kf$} map~$S$ from~$H$ to~$H$ such that the diagram
	\[
		\begin{tikzcd}[column sep = small, row sep = large]
			{}
			&
			H \tensor H
			\arrow{rr}[above]{S \tensor \id}
			&
			{}
			&
			H \tensor H
			\arrow{dr}[above right]{m}
			&
			{}
			\\
			H
			\arrow{ur}[above left]{\Delta}
			\arrow{rr}[above]{\varepsilon}
			\arrow{dr}[below left]{\Delta}
			&
			{}
			&
			\kf
			\arrow{rr}[above]{u}
			&
			{}
			&
			H
			\\
			{}
			&
			H \tensor H
			\arrow{rr}[above]{\id \tensor S}
			&
			{}
			&
			H \tensor H
			\arrow{ur}[below right]{m}
			&
			{}
		\end{tikzcd}
	\]
	commutes.
	This linear map~$S$\glsadd{antipode} is then the \defemph{antipode}\index{antipode} of~$(H, m, u, \Delta, \varepsilon)$.
\end{definition}


\begin{remark}
	We will see in the upcoming lecture that there exist at most one antipode. 
	This will justify speaking of \enquote{the} antipode.
\end{remark}


\begin{example}
	Let~$G$ be a group and let~$\kf[G]$\glsadd{group algebra} be the group~algebra\index{group algebra} of~$G$.
	This becomes a Hopf algebra via
	\[
		\Delta(g) = g \tensor g \,,
		\quad
		\varepsilon(g) = 1 \,,
		\quad
		S(g) = g^{-1}
	\]
	for every~$g \in G$.
\end{example}



\begin{definition}
	\leavevmode
	\begin{enumerate}
		\item
			A linear map between bialgebras is a \defemph{homomorphism of bialgebras}\index{homomorphism!of bialgebras} if it is both a homomorphism of algebras and a homomorphism of coalgebras.
		\item
			A linear map between Hopf~algebras is a \defemph{homomorphism of Hopf~algebras}\index{homomorphism!of Hopf algebras} if it is a homomorphism of bialgebras.
	\end{enumerate}
\end{definition}

% LECTURE 2

\begin{example}
	Let~$G$ be a group and consider its group algebra~$\kf[G]$\index{group algebra}.
	We can define a Hopf algebra structure on~$\kf[G]$ via
	\[
		\Delta(g) = g \tensor g \,,
		\quad
		\varepsilon(g) = 1 \,,
		\quad
		S(g) = g^{-1}
	\]
	for every~$g \in G$.
\end{example}





\section{A New Interpretation of the Antipode}

\begin{construction}
	Let~$(A, m, u)$ be an algebra and let~$(C, \Delta, \varepsilon)$ be a coalgebra.
	Then~$\Hom(C, A)$ becomes an algebra via the \defemph{convolution product}\index{convolution product} given by
	\[
		f * g
		\defined
		m \circ (f \tensor g) \circ \Delta \,.
		\glsadd{convolution product}
	\]
	The associativity of the convolution product follows from the associativity of the multiplication of~$A$ and the coassociativity of the comultiplication of~$C$
	The unit of~$\Hom(C, A)$ is given by the composite~$u \circ \varepsilon$.
	
	The antipode of a Hopf algebra~$H$ is the same as an inverse to~$\id_H$ in the convolution algebra~$\Hom(H, H)$.
\end{construction}


\begin{corollary}
	Every bialgebra admits at most one antipode.
	\qed
\end{corollary}


\begin{remark}
	We see from this that \enquote{being a Hopf algebra} is a property of a bialgebra.
\end{remark}


\begin{proposition}
	The antipode of a Hopf algebra is an anti-homomorpism\index{antipode!anti-homomorphism} of algebras.
\end{proposition}


\begin{proof}
	Consider~$f, g, m \in \Hom(H \tensor H, H)$ where~$m$ is the multiplication of~$H$, and~$f$ and~$g$ are given by
	\[
		f(x \tensor y) = S(y) S(x) \,,
		\quad
		g(x \tensor y) = S(xy)
	\]
	for all~$x, y \in H$.
	Then~$f$ is a right inverse to~$m$ in~$\Hom(H \tensor H, H)$ and that~$g$ is a left inverse to~$g$.
	Thus~$f = g$.
\end{proof}


\begin{corollary}
	Let~$H$ be a commutative Hopf algebra.
	Then~$S$ is an algebra homomorphism.
	\qed
\end{corollary}





\section{Graded Variants}

\begin{fluff}
	Let us give a typical example of how Hopf algebras arise in history:

	Given a topological space~$G$, we can consider the cohomology ring~$\Homology^\bullet(G, \kf)$\index{cohomology ring}.
	This becomes an algebra via the cup product
	\[
		\smile
		\colon
		\Hom^k(G, \kf) \times \Hom^l(G, \kf)
		\to
		\Hom^{k+l}(G, \kf)
	\]
	Suppose now that~$G$ is a topological group.
	The multiplication~$G \times G \to G$ induces a comultiplication
	\[
		\Homology^\bullet(G, \kf)
		\to
		\Homology^\bullet(G, \kf) \tensor \Homology^\bullet(G, \kf) \,.
	\]
	With this comultiplication,~$\Homology^\bullet(G, \kf)$ becomes somewhat of a Hopf algebra, alas not in the sense that we have considered so far.
\end{fluff}


\begin{definition}
	Let~$G$ be a group, or more generally a monoid.
	A~\defemph{\graded{$G$}}~ring\index{graded!ring} is a ring~$R$ together with a direct sum decomposition~$R = \bigoplus_{i \in G} R_i$ into abelian subgroups, such that for all~$i, j \in G$,
	\[
		R_i R_j \subseteq R_{ij} \,.
	\]
\end{definition}


\begin{remark}
	Let~$R$ be a~\graded{$G$} ring.
	The multiplicative neutral element~$1$ is contained in~$R_0$.
\end{remark}


\begin{definition}
	Let~$R$ be a~\graded{$G$} ring.
	\begin{enumerate}
		\item
			A~\defemph{graded~\module{$R$}}\index{graded!module} is an~\module{$R$}~$M$ together with a decomposition into abelian subgroups~$M = \bigoplus_{i \in G} M_i$ such that for all~$i, j \in G$,
			\[
				R_i M_j
				\subseteq
				M_{ij} \,.
			\]
		\item
			Let~$R$ be a graded ring and let~$M$ and~$N$ be two graded~\modules{$R$}.
			A homomorphism of modules~$f$ from~$M$ to~$N$ is a \defemph{homomorpism of graded modules} if~$f(M_i) \subseteq N_i$ for all~$i \in G$.
		\item
			A~\defemph{\graded{$G$}~\algebra{$\kf$}}\index{graded!algebra} is a~\algebra{$\kf$}~$A$ together with the structure of graded ring on~$A$ such that the image of~$\kf$ in~$A$ is contained in~$A_e$ (where~$e$ denotes the neutral element of~$G$).
		\item
			Suppose that~$G \in \{ \Natural, \Integer, \Integer / 2 \}$.
			The~\graded{$G$} ring~$R$ is \defemph{graded commutative}\index{graded commutative} if for any two homogeneous elements~$x$ and~$y$ of degrees~$n$ and~$m$ we have
			\[
				xy = (-1)^{nm} nm \,.
			\]
		\item
			A graded~\algebra{$\kf$} is \defemph{connected}\index{connected} if~$A_e = \kf$.
	\end{enumerate}
\end{definition}


\begin{remark}
	\begin{enumerate}
		\item
			Every ring~$R$ can be regarded as a~\graded{$G$} ring with~$R_e = R$.
			Similarly, every~\algebra{$\kf$}~$A$ can be regarded as a~\graded{$G$} ring with~$A_e = A$.
		\item
			Let~$A$ be a~\graded{$G$}~\algebra{$\kf$}.
			All summand~$A_i$ are closed under the multiplication of~$A_e$, and the image of~$\kf$ in~$A$ is contained in~$A_e$.
			It follows that all direct summands~$A_i$ are already~\submodules{$\kf$} of~$A$.
			Similarly, if~$M$ is a graded~\module{$A$}, then each direct summand~$M_i$ is a~\submodule{$\kf$} of~$M$.
	\end{enumerate}
\end{remark}


\begin{definition}
	Let~$G = \Integer/2 = \{ 0, 1\}$.
	\begin{enumerate}
		\item
			A~\graded{$G$}~\module{$\kf$}~$V$ is a~\defemph{supermodule}\index{super!-module}.
			The direct summand~$V_0$\glsadd{even part} of~$V$ it the \defemph{even part}\index{even part} of~$V$, and the direct summand~$V_1$\glsadd{odd part} of~$V$ is the \defemph{odd part}\index{odd part} of~$V$.
		\item
			Suppose that~$\kf$ is a field.
			A supermodule over~$\kf$ is a \defemph{super vector space}\index{super!vector space}
		\item
			A~\graded{$G$}~\algebra{$\kf$} is a \defemph{superalgebra}\index{super!algebra}
		\item
			A superalgebra~$A$ is \defemph{supercommutative}\index{super!commutative} if it is graded commutative.
	\end{enumerate}
\end{definition}


\begin{example}
	Let~$V$ be a vector space over a field~$\kf$.
	The exterior algebra~$\Exterior(V)$\index{exterior algebra} carries the structure of a graded~\algebra{$\kf$} via the decomposition~$\Exterior(V) = \bigoplus_{n \in \Natural} \Exterior^n(V)$.
	This graded algebra is graded commutative.
\end{example}


\begin{definition}
	Let~$V$ and~$W$ be graded two~\modules{$\kf$}.
	The \defemph{induced grading} on the tensor product~$V \tensor W$ is given by~$V \tensor W = \bigoplus_{i \in G} (V \tensor W)_i$ with
	\[
		(V \tensor W)_i
		=
		\bigoplus_{jk = i} V_j W_k \,.
	\]
\end{definition}


\begin{definition}
	\leavevmode
	\begin{enumerate}
		\item
			A~\defemph{\graded{$G$}}~\coalgebra{$\kf$}\index{graded!coalgebra} consists of a graded~\module{$\kf$}~$C$ together with the structure of a~\coalgebra{$\kf$} on~$C$ such that both the comultiplication and the counit of~$C$ are homomorphisms of graded~\modules{$\kf$}.%
			\footnote{
				Here we regard~$\kf$ as a graded~\module{$\kf$} with~$\kf_e = \kf$.
			}
		\item
			A~\defemph{\graded{$G$}~\bialgebra{$\kf$}}\index{graded!algebra} consists of a~\graded{$G$}~\module{$\kf$}~$B$ together with the structure of a~\bialgebra{$\kf$} on~$B$ such the unit, multiplication, counit, and comultiplication of~$B$ are homomorphisms of graded~\modules{$\kf$}.
		\item
			A~\defemph{\graded{$G$}}~\hopfalgebra{$\kf$}\index{graded!Hopf algebra} consists of a graded~\graded{$G$}~\module{$\kf$}~$H$ togehter with the structure of a~\hopfalgebra{$\kf$} on~$H$ such that the multiplication, unit, comultiplication, counit, and 
		\item
			A homomorphism of graded~\algebras{$\kf$} (resp.~\coalgebras{$\kf$},~\bialgebras{$\kf$},~\hopfalgebras{$\kf$}) is a homorphism of algebras that is also a homomorphism of graded~\modules{$\kf$}.
	\end{enumerate}
\end{definition}


\begin{warning}
	When topologists talk about Hopf~algebras, they often mean graded Hopf~algebras.
	They also sometimes don’t require the existence of an antipode in their definition of a~Hopf algebra (i.e. they don’t distinguish between graded bialgebras and graded Hopf~algebras).
	This is due to the fact that every connected, graded bialgebra admits an antipode.
\end{warning}





\section{The Universal Enveloping Algebra}

\begin{example}
	Let~$\kf$ be a field and let~$\glie$ be a~\liealgebra{$\kf$}.
	Let~$\Univ(\glie)$ be the universal enveloping algebra\index{universal enveloping algebra} of~$\glie$.
	The diagonal map
	\[
		\delta
		\colon
		\glie
		\to
		\glie \oplus \glie \,,
		\quad
		x
		\mapsto
		(x, x)
	\]
	is a homomorphism of Lie~algebras and therefore induces an isomorphism of algebras
	\[
		\Univ(\delta)
		\colon
		\Univ(\glie)
		\to
		\Univ(\glie \oplus \glie) \,.
	\]
	Under the natural isomorphism~$\Univ(\glie \oplus \glie) \cong \Univ(\glie) \tensor \Univ(\glie)$, the homomorphism~$\Univ(\delta)$ corresponds to a homomorphism of algebras
	\[
		\Delta
		\colon
		\Univ(\glie)
		\to
		\Univ(\glie) \tensor \Univ(\glie)
	\]
	that is given by
	\[
		\Delta(x)
		=
		x \tensor 1 + 1 \tensor x
	\]
	for all~$x \in \Univ(\glie)$.
	The zero map
	\[
		\glie
		\to
		0
	\]
	is a homomorphism of Lie~algebras and therefore induces a homomorphism of algebras
	\[
		\varepsilon
		\colon
		\Univ(\glie)
		\to
		\Univ(0)
		=
		\kf \,,
	\]
	given by~$\varepsilon(x) = 0$ for every~$x \in \glie$.
	The map
	\[
		\glie
		\to
		\glie \,,
		\quad
		x
		\mapsto
		-x
	\]
	is a an anti-homomorphism of Lie~algebras and therefore induces an anti-homomorphism of algebras
	\[
		S
		\colon
		\Univ(\glie)
		\to
		\Univ(\glie)
	\]
	that is given by~$S(x) = -x$ for every~$x \in \glie$.

	This additional maps make~$\Univ(\glie)$ into a Hopf~algebra.
\end{example}


\begin{example}
	Let~$V$ be a vector space over~$\kf$.
	\begin{enumerate}
		\item
			We may regard~$V$ as an abelian Lie~algebra.
			Its universal enveloping algebra is then given by its symmetric algebra~$\Symm(V)$\index{symmetric algebra}.
			We can therefore endow~$\Symm(V)$ with the structure of a Hopf~algebra.
		\item
			We can similarly regard the tensor algebra~$\Tensor(V)$\index{tensor algebra} as the universal enveloping algebra of~$\flie(V)$, the free Lie~algebra\index{free Lie algebra} on~$V$.
			We can therefore endow~$\Tensor(V)$ with the structure of a Hopf~algebra.
	\end{enumerate}
\end{example}


\begin{fluff}
	This kind of Hopf~algebra is far from rare, at the following~\lcnamecref{milnor-more} shows.
\end{fluff}


\begin{definition}
	Let~$B$ be a bialgebra.
	An element~$x$ of~$B$ is \defemph{primitive}\index{primitive element} if~$\Delta(x) = x \tensor 1 + 1 \tensor x$.
\end{definition}


\begin{example}
	Let~$\glie$ be a Lie~algebra.
	Every element of~$\glie$ is primitive in~$\Univ(\glie)$.
\end{example}


\begin{remark}
	If~$\glie$ as a Lie~algebra over a field of characteristic zero, then the elements of~$\glie$ are the only primitive elements of~$\glie$.
\end{remark}


\begin{proposition}
	Let~$B$ be a bialgebra.
	The set of primitive elements of~$B$ is a Lie~subalgebra of~$B$.
	\qed
\end{proposition}


\begin{theorem}[Milnor--More]\index{Milnor--More}
	\label{milnor-more}
	Let~$H$ be a cocommutative, connected Hopf~algebra over a field of characteristic zero.
	Let~$\glie$ be the Lie~algebra of primitive elements of~$H$.
	Then~$H$ is isomorphic to~$\Univ(\glie)$.
\end{theorem}


\section{The Graded World}

\begin{fluff}
	\label{different definitions of graded lie algebras}
	Let~$\kf$ be a field and let~$G$ be one of the two groups~$\Integer$ or~$\Integer/2$.
	We want to say what a graded~\liealgebra{$\kf$} is.
	The problem is that there are three possible definitions one can consider.
	\begin{enumerate*}
		\item
			A graded Lie~algebra over~$\kf$ is a Lie~algebra~$\glie$ together with a~\grading{$G$} of vector spaces such that~$[\glie_i, \glie_j] \subseteq \glie_{i+j}$.
		\item
			A graded Lie~algebra over~$\kf$ is a~\graded{$G$} vector space~$\glie$ together with a homomorphism of graded vector spaces
			\[
				[\ph, \ph]
				\colon
				\glie \tensor \glie
				\to
				\glie
			\]
			such that there exist an associative, graded~\algebra{$\kf$}~$A$ and an injective homomorphism of graded vector spaces~$j$ from~$\glie$ to~$A$ such that for all~$x, y \in \glie$,
			\[
				j( [x, y] )
				=
				[ j(x), j(y) ] \,.
			\]
			(The bracket on~$A$ is given by~$[a, b] = ab - (-1)^{\Deg{a}\Deg{b}} ba$ for all homogeneous elements~$a$ and~$b$ of~$\glie$.)
		\item
			\label{our definition of graded lie algebras}
			A graded Lie~algebra over~$\kf$ is a~\graded{$G$} vector space~$\glie$ together with a homomorphism of graded vector spaces
			\[
				[\ph, \ph]
				\colon
				\glie \tensor \glie
				\to
				\glie
			\]
			such that
			\begin{enumerate}
				\item
					$[x, y] = - (-1)^{\Deg{x}\Deg{y}} [y, x]$ for all homogeneous elements~$x$ and~$y$ of~$\glie$.
				\item
					For any three homogeneous elements of~$\glie$ the \defemph{graded Jacobi identity}\index{graded Jacobi identity} holds:
					\[
						[ x, [y, z] ]
						=
						[ [x, y], z ]
						+
						(-1)^{\Deg{x} \Deg{y}} [ y, [x, z] ] \,.
					\]
			\end{enumerate}
	\end{enumerate*}

	The last two definitions are equivalent if the field~$\kf$ is not of characteristic~two or~three.
	We will use the third definition.
\end{fluff}

\begin{definition}
	Let~$\kf$ be a field an let~$G$ be one of the two groups~$\Integer$ or~$\Integer/2$.
	A~\defemph{\graded{$G$}}~\liealgebra{$\kf$}\index{graded Lie algebra} is defined as in~\ref{our definition of graded lie algebras} of \cref{different definitions of graded lie algebras}.
\end{definition}


\begin{definition}
	Let~$\glie$ and~$\hlie$ be two graded Lie~algebras.
	A homomorphism of graded vector spaces~$f$ from~$\glie$ to~$\hlie$ is a \defemph{homomorphism of graded Lie~algebras} if it makes the following diagram commute:
	\[
		\begin{tikzcd}[sep = large]
			\glie \tensor \glie
			\arrow{r}[above]{[\ph, \ph]}
			\arrow{d}[left]{f \tensor f}
			&
			\glie
			\arrow{d}[right]{f}
			\\
			\hlie \tensor \hlie
			\arrow{r}[above]{[\ph, \ph]}
			&
			\hlie
		\end{tikzcd}
	\]
\end{definition}


\begin{definition}
	A~\graded{$(\Integer/2)$} Lie~algebra is a \defemph{Lie~superalgebra}\index{Lie superalgebra}.
\end{definition}
























