These are my personal notes for the lecture course _Hopf Algebras, Quantum Groups and Nichols Algebras_ that is given by Dr. Thorsten Heidersdorf at the University of Bonn in the summer term of 2021.

A compiled version of these notes can be found at [https://lecture-notes-bonn.gitlab.io/original/hopf-quantum-nichols-notes-ss21/hopf-quantum-nichols.pdf][1]

[1]: https://lecture-notes-bonn.gitlab.io/original/hopf-quantum-nichols-notes-ss21/hopf-quantum-nichols.pdf
